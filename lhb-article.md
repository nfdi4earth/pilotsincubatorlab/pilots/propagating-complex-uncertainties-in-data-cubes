---
name: Propagating complex uncertainties in data cubes
# The title of the article.

description: 
Analytically intractable uncertainty structures occur across the Earth system sciences when numerical simulations and point measurements are compared. Using paleoclimatology as example, our python package cupsm integrates paleoclimate proxy data into a data cube architecture and implements operators mapping between simulation output and proxy measurements, using Monte Carlo methods for uncertainty propagation. A tutorial demonstrates our approach by comparing transient simulations of the last 25,000 years with a database of sea surface temperature reconstructions. cupsm improves interoperability and reusability of data analysis workflows in paleoclimatology.
# A brief summary of the article, max. 300 characters incl. spaces.

author: 
  - name: Nils Weitzel
    orcidid: https://orcid.org/0000-0002-2735-2992
  - name: Muriel Racky
    orcidid: https://orcid.org/0000-0003-0048-0131
  - name: Laura Braschoss
    orcidid: https://orcid.org/0009-0001-2370-4532
  - name: Kire Rehfeld
    orcidid: https://orcid.org/0000-0002-9442-5362
#toDo: replace the authors with the project coworkers, the orcid is optional for all further author but mandatory for the first author.
# The author(s) of the article. The name must be given, the ORCID should be given. As many authors as necessary can be listed. 


language: ENG
# The language of the article. Accepted values are:  
# ENG for English
# DEU for German

collection: 
  - N4E_Pilots.md
# The filenames of the NFDI4Earth Living Handbook collection(s), the article belongs to, as they appear in https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/tree/main/docs. 
# Alternatively, the titles of the collections. The editor will replace it with the collections' file names. 

subtype: technical_note
# The type of the article. Acceptable terms are: 
# article: The most generic category. Should only be used if none of the others fit. 
# best_practice: A procedure of how a defined problem described in the article can be solved which is a (de-facto) standard or widely accepted in the community
# workflow: A step-by-step description of a procedure
# technical_note: A short article presenting a new software tool, method, technology. 
# recommended_article: a short review of a publication.
# discussion/review: An article that summarises/compares or (critically) discusses e.g., the state of understanding, different approaches or methodologies of a topic. 
# user_guide: Manuals on how to use e.g., software products. 
# collection: A NFDI4Earth Living Handbook collection article.
# FAQ: Can only used by the NFDI4Earth User Support Network, which define the FAQs. 

subject: 
  - dfgfo:313-01
  - dfgfo:313-02
  - unesco:concept5452
  - unesco:concept183
  - unesco:concept7121
  - unesco:concept2214
#toDO: adapt the subject to your project subject
# A list of controlled terms describing the subject of the article, given as their identifiers. Acceptable are any terms from: 
# DFG subject ontology: https://terminology.tib.eu/ts/ontologies/dfgfo/terms
# UNESCO thesaurus: https://vocabularies.unesco.org/browser/thesaurus/en/

keywords:
  - paleoclimate
  - data-cubes
  - proxy-system-modeling
  - model-data-comparison
#toDo: adapt the kewords
# A list of terms to describe the article's topic more precisely. 

target_role: 
  - data user
  - data curator
  - data collector
# A list of the main roles of persons within research data management that is addressed by the article. 
# data collector: The person responsible for collecting, generating, re-using or contributing to research data and documents contexts and processes that relates to data handling. 
# data owner: Typically the principal researcher of the project or research group leader who is responsible for establishing who has access to the data, and reducing or mitigating the risk of any mishaps; e.g. improper storage facility, non-compliance with specific guidelines. 
# data depositor: Either the data creator or data owner and is the person who will perform the deposit of the data to a data preservation facility, like a repository or data archive. They perform activities such as uploading data after it has been generated, making it suitable for archiving, creating metadata and data documentation and submitting the data to the facility, so it can be preserved for the long-term. 
# data user: Someone who is interested in accessing and interacting with data for verification or (re)using data to evaluate, analyse, model and generally handle then to create insights and knowledge. access to research data for re-use or verification. 
# data curator: A person who ensures availability and reliability of data. They review and harmonise metadata of datasets submitted for storage and ensure that they are generally compliant with relevant policies before publication. They further monitor access to the data, support data owners in fulfilling data access requests and possess deeper technical knowledge about the backend of the repository or storage system. 
# data librarian: An information professional responsible for developing technical expertise on practical solutions of data management, archiving and dissemination and on data mining and visualization. 
# data steward: A supporter who advises researchers (or otherwise involved) persons on data management, privacy and information security aspects and can understand the meaning and applications of the data. They support: data creators on quality assurance, storage, policy compliance; data depositors on data selection, metadata and data documentation creation, preserving and publishing; data custodians on data curation, access controls, FAIR data management 
# data advocate: A person promoting the proper use of research data and technology and providing assistance or guidance in doing so. 
# data provider: A person or organization that stores and provides datasets to others.
# service provider: An organization that provides services related to the processing and management of data, such as, high-performance computing facilities or web-based maps.
# research software engineer: A person working with researchers to gain an understanding of the problems they face, and then develops, maintains and extends software to provide the answers. 
# policy maker: In research data management, policy makers strive to design effective, technology-neutral, forward-looking and coherent data governance policies across sectors.
# general public: All persons that do not belong to any of the other groups. 

# identifier: 
# If the article has already a unique identifier such as an DOI, provide it here as URL (e.g., https://doi.org/10.1001/12345). 

version: 0.1
# The version of the article; will be updated by the editor. 

#toDo: convert your project description to markdown by copying the text and adapting headings, links and reference. References could be stored in the bib.bib file in bibtex format and can cites as discripted in th LHB (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines/#references)
---

# Propagating complex uncertainties in data cubes

## Introduction

Numerical models are a fundamental tool for understanding the Earth system. To test the reliability of models, they need to be compared with observational data. This task often involves operators that map model output onto the measurement space. In paleoclimatology, so-called proxy system models (PSMs) map Earth system model (ESM) output onto proxy measurements from natural climate archives such as ice and sediment cores ([@evans_applications_2013]). PSMs consist of process chains with multiple sources of autocorrelated and non-Gaussian uncertainties ([@dolman_sedproxy_2018]). Over time, the amounts of ESM data available for analysis has increased substantially, which fostered the development of efficient processing methods on data cubes for gridded ESM output in python (e.g., [@eyring_earth_2020; @may_metpy_2022]).

Meanwhile, paleoclimate proxy data is mostly stored in a variety of tabular and database formats. This is because the collection of paleoclimate proxies is an interdisciplinary process with different communities following different data management and metadata reporting conventions (e.g., [@jonkers_integrating_2020; @sanchez_goni_acer_2017]). A suite of processing tools for proxy data exists (e.g., [@blaauw_flexible_2011; @tierney_bayspline_2018; @dolman_sedproxy_2018]), but they are usually not designed to perform computations efficiently on large datasets because the size of individual proxy record datasets is comparatively small. However, uncertainties of paleoclimate proxies tend to be non-Gaussian and autocorrelated, which makes them analytically intractable. Therefore, Monte Carlo methods are convenient for rigorous uncertainty quantification. Together with more comprehensive proxy record databases, this creates a need for incorporating big data methods into the evaluation of ESMs against paleoclimate proxies.

## Key features

To leverage the progress in ESMs, global proxy databases, and PSMs for climate process understanding and model validation, we developed the python package *cupsm* which builds on on existing [*xarray*](https://docs.xarray.dev/en/stable/) ([@hoyer_xarray_2017]) and [*dask*](https://www.dask.org) functionalities. Key features of *cupsm* are

- python objects for transient paleoclimate simulations (`sim_data`), collections of proxy records (`obs_data`), and proxy data from individual measurement sites (`site_object`), which are suitable for parallelization and lazy loading with xarray and dask;
- implementations for three types of operators: space operators (`field2site`) that map the spatial fields of the `sim_data` onto the spatial structure of the `site_objects`, chronology operators (`time2chron`) that map data from the regular `sim_data` time axis onto the irregular `site_object` time axis, and exemplifying variable operators that perturb the data in `sim_data` or `site_objects` with noise to imitate proxy archival processes;
- tutorials to explore the objects and operators and guide users through the steps of comparing simulated and reconstructed sea surface temperatures from a transient simulation of the last 26,000 years ([@kapsch_ocean_2022]) and a collection of proxy records from marine sediment cores, the “PALMOD 130k marine palaeoclimate data synthesis V1_0_1” ([@jonkers_integrating_2020]).

The main user group of *cupsm* are (paleo)climatologists, in particular Earth system modelers and paleoclimate proxy experts. Using harmonized data structures for paleoclimate simulations and proxies in *cupsm* makes ESM output and proxy data more accessible, in particular to users who are not working with the respective data types regularly. Therefore, *cupsm* improve interoperability and reusability of data analysis workflows in paleoclimatology by using consistent and efficient methods for all processing steps in model-proxy comparison.

## Future Directions

Future work and recommended directions towards model-to-measurement data cubes with rigorous uncertainty propagation can be divided into three aspects: short-term plans, mid-term goals, and long-term visions. In the short-term, further development and maintenance of *cupsm* is ensured through project funding of Nils Weitzel. Future developments will focus on the extension of the available PSM operators and integrated datasets, and the implementation of probabilistic divergence functions, with the goal of improved evaluations of simulations from the [PMIP](https://pastglobalchanges.org/science/wg/cvas/intro) and [PalMod](palmod.de) projects.

The mid-term goal is establishing complete model-to-measurement data cube workflows with rigorous uncertainty propagation in (paleo)climatology, and integrate these *cupsm* functionalities into an existing ESM evaluation tool for simulations of the instrumental period. In the long-term, the vision of model-to-measurement data cubes with rigorous uncertainty propagation could be adopted by other Earth system science disciplines. This is possible since the concepts of this pilot can be applied to all measurement operators involving analytically intractable uncertainty structures.

`![Structure of cupsm objects](cupsm_objects.png "Measurement operators translate the sim_data (top, given by a chunked xarray DataArray for the variable ‘tos’, which is the CMOR variable name of SST, with space and time dimensions) into an obs_data object (bottom), which is given by N site_objects. Each site_object contains an xarray Dataset with dimensions depth and ens.")`

`![cupsm example](rec_psm.png "Example of a 26,000 years long transient simulation of the last deglaciation with MPI-ESM (gray) and reconstructed SST anomalies from proxies archived in five marine sediment cores (green). For each proxy record, a habitat season is selected and the simulation is downsampled to the proxy time axes, with quantification of chronological uncertainties (blue).")`

## Resources

- [Pilot Roadmap](https://doi.org/10.5281/zenodo.14627370)[@weitzel_towards_2025]
- Code repository ([github](https://github.com/paleovar/cupsm/tree/main) | [zenodo](https://doi.org/10.5281/zenodo.14624116)[@weitzel_cupsm_2025])
- [cupsm documentation](https://cupsm.readthedocs.io/en/latest/index.html)

## References

