# Propagating complex uncertainties in data cubes

## Introduction

Numerical models are a fundamental tool for understanding the Earth system. To test the reliability of models, they need to be compared with observational data. This task often involves operators that map model output onto the measurement space. In paleoclimatology, so-called proxy system models (PSMs) map Earth system model (ESM) output onto proxy measurements from natural climate archives such as ice and sediment cores (Evans et al., 2013). PSMs consist of process chains with multiple sources of autocorrelated and non-Gaussian uncertainties (Dolman and Laepple, 2018). Over time, the amounts of ESM data available for analysis has increased substantially, which fostered the development of efficient processing methods on data cubes for gridded ESM output in python (e.g., Eyring et al., 2020; May et al., 2022).

Meanwhile, paleoclimate proxy data is mostly stored in a variety of tabular and database formats. This is because the collection of paleoclimate proxies is an interdisciplinary process with different communities following different data management and metadata reporting conventions (e.g., Jonkers et al., 2020; Sánchez Goñi et al., 2017). A suite of processing tools for proxy data exists (e.g., Blaauw and Christen, 2011; Tierney and Tingley, 2018; Dolman and Laepple, 2018), but they are usually not designed to perform computations efficiently on large datasets because the size of individual proxy record datasets is comparatively small. However, uncertainties of paleoclimate proxies tend to be non-Gaussian and autocorrelated, which makes them analytically intractable. Therefore, Monte Carlo methods are convenient for rigorous uncertainty quantification. Together with more comprehensive proxy record databases, this creates a need for incorporating big data methods into the evaluation of ESMs against paleoclimate proxies.


## Objectives

To leverage the progress in ESMs, global proxy databases, and PSMs for climate process understanding and model validation, the primary objectives of our pilot are
- to develop flexible data objects in python for collections of paleoclimate proxy data and transient climate simulations that make use of data cube concepts;
- to implement operators that map ESM output onto proxy measurements with a rigorous propagation of uncertainties through Monte Carlo methods;
- it improve interoperability and reusability of data analysis workflows in paleoclimatology by using consistent and efficient methods for all processing steps in model-proxy comparison.


## Key Features

Building on existing [xarray](https://docs.xarray.dev/en/stable/) (Hoyer and Hamman, 2017) and [dask](https://www.dask.org) functionalities, our python package *cupsm* contains 
- python objects for transient paleoclimate simulations (`sim_data`), collections of proxy records (`obs_data`), and proxy data from individual measurement sites (`site_object`), which are suitable for parallelization and lazy loading with xarray and dask;
- implementations for three types of operators: space operators (`field2site`) that map the spatial fields of the `sim_data` onto the spatial structure of the `site_objects`, chronology operators (`time2chron`) that map data from the regular `sim_data` time axis onto the irregular `site_object` time axis, and exemplifying variable operators that perturb the `sim_data` with noise to imitate proxy archival processes;
- tutorials to explore the objects and operators, and to guide users through the steps of comparing simulated and reconstructed sea surface temperatures from a transient simulation of the last 26,000 years (Kapsch et al., 2022) and a collection of proxy records from marine sediment cores, the “PALMOD 130k marine palaeoclimate data synthesis V1_0_1” (Jonkers et al., 2020).


## Outcomes

The results of our pilot have been presented at [EGU2024](https://doi.org/10.5194/egusphere-egu24-10541), at the NFDI4Earth plenum 2024, and within the NFDI4Earth Academy. The pilot resulted in the following publications: 
- The roadmap ["Towards model-to-measurement data cubes with rigorous uncertainty propagation"](https://doi.org/10.5281/zenodo.14627370) summarizes the background and results of our pilot as well as recommendations for future work. 
- The python package [*cupsm*](https://doi.org/10.5281/zenodo.14624116) contains all code developments of our pilot. The latest version is available on [github](https://github.com/paleovar/cupsm/tree/main).
- The [documentation](https://cupsm.readthedocs.io) of *cupsm* on readthedocs explains the concepts and objects of *cupsm*, contains tutorials, and documents the *cupsm* API.


## Challenges and Gaps

Two of the implemented functionalities were more time-consuming than planned: (1) reading, cleaning, and processing the SST proxy database required more individual time than expected, and (2) implementing operators that efficiently parallelize tasks with dask was more challenging than expected, in particular when trying to mix existing parallelized *xarray* functions with functions that are parallelized with *dask* by hand. Therefore, two of the planned tasks could not be implemented due to time limitations. These are (1) testing the efficiency of integrating existing PSM functionalities in *R* through a parallelized python wrapper, and (2) implementing metrics for measuring the deviation between forward-model proxy time series and paleoclimate proxies. The latter was conceptually demonstrated in a paper, which was published during the project duration (Weitzel et al. 2024), but is not yet integrated into *cupsm*.


## Relevance for the Community

The development of *cupsm* is connected to national and international activities that the PIs Nils Weitzel and Kira Rehfeld are involved in, in particular [PalMod](palmod.de), [PMIP (Paleoclimate Model Intercomparison Project](pmip4.lsce.ipsl.fr), [natESM](nat-esm.de), and [PAGES CVAS (Climate Variability across Scales)](https://pastglobalchanges.org/science/wg/cvas/intro). The main user group of *cupsm* are (paleo)climatologists, in particular Earth system modelers and paleoclimate proxy experts. Using harmonized data structures for paleoclimate simulations and proxies in *cupsm* makes ESM output and proxy data more accessible, in particular to users who are not working with the respective data types regularly. Furthermore, scientists from other Earth system science disciplines, in which uncertainty quantification for large datasets are important, could integrate our concepts into their workflows. Our vision also emphasizes the benefit of using file formats in data repositories that are compatible with big data concepts, even if the individual datasets are comparatively small.


## Future Directions

Future work and recommended directions towards model-to-measurement data cubes with rigorous uncertainty propagation can be divided into three aspects: short-term plans, mid-term goals, and long-term visions. In the short-term, further development and maintenance of *cupsm* is ensured through project funding of Nils Weitzel. Future developments will focus on the extension of the available operators and integrated datasets, and the implementation of probabilistic divergence functions, with the goal of improved evaluations of simulations from the [PMIP](https://pastglobalchanges.org/science/wg/cvas/intro) and [PalMod](palmod.de) projects.

The mid-term goal is establishing complete model-to-measurement data cube workflows with rigorous uncertainty propagation in (paleo)climatology, and integrate *cupsm* into an existing ESM evaluation tool for simulations of the instrumental period. In the long-term, the vision of model-to-measurement data cubes with rigorous uncertainty propagation could be adopted by other Earth system science disciplines. This is possible since the concepts of this pilot can be applied to all measurement operators involving analytically intractable uncertainty structures.


## Contributors

Nils Weitzel, [https://orcid.org/0000-0002-2735-2992](https://orcid.org/0000-0002-2735-2992)
Muriel Racky, [https://orcid.org/0000-0002-9442-5362](https://orcid.org/0000-0002-9442-5362)
Laura Braschoss, [https://orcid.org/0009-0001-2370-4532](https://orcid.org/0009-0001-2370-4532)
Kira Rehfeld, [https://orcid.org/0000-0002-9442-5362](https://orcid.org/0000-0002-9442-5362)


## Funding

This work has been funded by the German Research Foundation (NFDI4Earth, DFG project no. 460036893, [https://www.nfdi4earth.de](https://www.nfdi4earth.de)).


## References

- Blaauw, M. and Christen, J. A.: Flexible paleoclimate age-depth models using an autoregressive gamma process, Bayesian Anal., 6, [https://doi.org/10.1214/11-BA618](https://doi.org/10.1214/11-BA618), 2011.
- Dolman and Laepple: Sedproxy: a forward model for sediment-archived climate proxies, Clim. Past, 14,  [https://doi.org/10.5194/cp-14-1851-2018](https://doi.org/10.5194/cp-14-1851-2018), 2018.
- Evans, M. N., Tolwinski-Ward, S. E., Thompson, D. M., and Anchukaitis, K. J.: Applications of proxy system modeling in high resolution paleoclimatology, Quaternary Science Reviews, 76, 16–28, [https://doi.org/10.1016/j.quascirev.2013.05.024](https://doi.org/10.1016/j.quascirev.2013.05.024), 2013.
- Eyring, V., Bock, L., Lauer, A., Righi, M., Schlund, M., Andela, B., Arnone, E., Bellprat, O., Brötz, B., Caron, L.-P., Carvalhais, N., Cionni, I., Cortesi, N., Crezee, B., Davin, E. L., Davini, P., Debeire, K., de Mora, L., Deser, C., Docquier, D., Earnshaw, P., Ehbrecht, C., Gier, B. K., Gonzalez-Reviriego, N., Goodman, P., Hagemann, S., Hardiman, S., Hassler, B., Hunter, A., Kadow, C., Kindermann, S., Koirala, S., Koldunov, N., Lejeune, Q., Lembo, V., Lovato, T., Lucarini, V., Massonnet, F., Müller, B., Pandde, A., Pérez-Zanón, N., Phillips, A., Predoi, V., Russell, J., Sellar, A., Serva, F., Stacke, T., Swaminathan, R., Torralba, V., Vegas-Regidor, J., von Hardenberg, J., Weigel, K., and Zimmermann, K.: Earth System Model Evaluation Tool (ESMValTool) v2.0 – an extended set of large-scale diagnostics for quasi-operational and comprehensive evaluation of Earth system models in CMIP, Geosci. Model Dev., 13, 3383–3438, [https://doi.org/10.5194/gmd-13-3383-2020](https://doi.org/10.5194/gmd-13-3383-2020), 2020.
- Hoyer and Hamman: xarray: N-D labeled Arrays and Datasets in Python,  J. Open Source Softw., 5, [https://doi.org/10.5334/jors.148](https://doi.org/10.5334/jors.148), 2017.
- Jonkers, L., Cartapanis, O., Langner, M., McKay, N., Mulitza, S., Strack, A., and Kucera, M.: Integrating palaeoclimate time series with rich metadata for uncertainty modelling: strategy and documentation of the PalMod 130k marine palaeoclimate data synthesis, Earth Syst. Sci. Data, 12, 1053–1081, [https://doi.org/10.5194/essd-12-1053-2020](https://doi.org/10.5194/essd-12-1053-2020), 2020.
- Kapsch, M., Mikolajewicz, U., Ziemen, F., and Schannwell, C.: Ocean Response in Transient Simulations of the Last Deglaciation Dominated by Underlying Ice‐Sheet Reconstruction and Method of Meltwater Distribution, Geophysical Research Letters, 49, [https://doi.org/10.1029/2021GL096767](https://doi.org/10.1029/2021GL096767), 2022.
- May, R. M., Goebbert, K. H., Thielen, J. E., Leeman, J. R., Camron, M. D., Bruick, Z., Bruning, E. C., Manser, R. P., Arms, S. C., and Marsh, P. T.: MetPy: A Meteorological Python Library for Data Analysis and Visualization, Bulletin of the American Meteorological Society, 103, E2273–E2284, [https://doi.org/10.1175/BAMS-D-21-0125.1](https://doi.org/10.1175/BAMS-D-21-0125.1), 2022.
- Sánchez Goñi, M. F., Desprat, S., Daniau, A.-L., Bassinot, F. C., Polanco-Martínez, J. M., Harrison, S. P., Allen, J. R. M., Anderson, R. S., Behling, H., Bonnefille, R., Burjachs, F., Carrión, J. S., Cheddadi, R., Clark, J. S., Combourieu-Nebout, N., Mustaphi, Colin. J. C., Debusk, G. H., Dupont, L. M., Finch, J. M., Fletcher, W. J., Giardini, M., González, C., Gosling, W. D., Grigg, L. D., Grimm, E. C., Hayashi, R., Helmens, K., Heusser, L. E., Hill, T., Hope, G., Huntley, B., Igarashi, Y., Irino, T., Jacobs, B., Jiménez-Moreno, G., Kawai, S., Kershaw, A. P., Kumon, F., Lawson, I. T., Ledru, M.-P., Lézine, A.-M., Liew, P. M., Magri, D., Marchant, R., Margari, V., Mayle, F. E., McKenzie, G. M., Moss, P., Müller, S., Müller, U. C., Naughton, F., Newnham, R. M., Oba, T., Pérez-Obiol, R., Pini, R., Ravazzi, C., Roucoux, K. H., Rucina, S. M., Scott, L., Takahara, H., Tzedakis, P. C., Urrego, D. H., van Geel, B., Valencia, B. G., Vandergoes, M. J., Vincens, A., Whitlock, C. L., Willard, D. A., and Yamamoto, M.: The ACER pollen and charcoal database: a global resource to document vegetation and fire response to abrupt climate changes during the last glacial period, Earth Syst. Sci. Data, 9, 679–695, [https://doi.org/10.5194/essd-9-679-2017](https://doi.org/10.5194/essd-9-679-2017), 2017.
- Tierney, J. E. and Tingley, M. P.: BAYSPLINE: A New Calibration for the Alkenone Paleothermometer, Paleoceanography and Paleoclimatology, 33, 281–301, [https://doi.org/10.1002/2017PA003201](https://doi.org/10.1002/2017PA003201), 2018.
- Weitzel, N., Andres, H., Baudouin, J.-P., Kapsch, M.-L., Mikolajewicz, U., Jonkers, L., Bothe, O., Ziegler, E., Kleinen, T., Paul, A., and Rehfeld, K.: Towards spatio-temporal comparison of simulated and reconstructed sea surface temperatures for the last deglaciation, Clim. Past, 20, 865–890, [https://doi.org/10.5194/cp-20-865-2024](https://doi.org/10.5194/cp-20-865-2024), 2024.
